---- Pekan 4 Hari 3 - Berlatih SQL Join

1. Mengambil Data dari Database

a. Mengambil data users
Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.


select id, name, email from users;


b. Mengambil data items
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

select  * from items where price > 1000000;


Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

select * from items where name like '%uniklo%';


c. Menampilkan data items join dengan kategori
Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items. Berikut contoh tampilan data yang ingin didapatkan

select items.name, categories.name from items inner join categories on items.category_id = categories.id;


2.  Mengubah Data dari Database
Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 2.

update items set price= 2500000 where id=1;



---- Pekan 4 Hari 2 - Berlatih SQL Dasar
1.buat Database

create database myshop;


2. Membuat Table di Dalam Database

use myshop

// table users

create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

// table categories

create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

// table items

create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(8),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );


3. Memasukkan Data pada Table

// table users

insert into users(name,email,password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

// table categories

insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

// table items

insert items(name,description,price,stock,category_id) values ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1), ("Uniklooh","baju keren dari brand ternama",500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil Data dari Database

- mengambil data items kecuali category_id

select name, description, price, stock from items;

- mengambil data item dengan kondisi price > 1000000

select  * from items where price > 1000000;

- mengambil data item dengan deskripsi keren menggunakan Like

select * from items where description like '%keren%';

- mengambil data di kedua table menggunakan join, data nama items dengan nama categories

select items.name, categories.name from items inner join categories on items.category_id = categories.id;


5. Mengubah Data dari Database

update harga pada tabel items, Uniklooh dari 500000 menjadi 600000

update items set price= 600000 where id=2;


